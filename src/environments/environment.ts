// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyDDWidJ_bUFzZ6VJEU1eK6UEfqO0XC61pQ",
    authDomain: "signageninja-dev.firebaseapp.com",
    databaseURL: "https://signageninja-dev.firebaseio.com",
    projectId: "signageninja-dev",
    storageBucket: "signageninja-dev.appspot.com",
    messagingSenderId: "257834332485"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
