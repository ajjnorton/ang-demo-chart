
# SignageNinja02

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.7.

## Phase 1 - Angular setup

## Prerequistes
Both the CLI and generated project have dependencies that require Node 8.9 or higher, together with NPM 5.5.1 or higher.


## Install Angular CLI
npm install -g @angular/cli

## Create a new app
ng new my-new-app --style=scss

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding
Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests
Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Phase 2 - Firebase setup
Install firebase and AngularFire2
```
npm install firebase angularfire2 —-save
```

import firebase into app.module
```
import { AngularFirestore } from 'angularfire2/firestore';
```
